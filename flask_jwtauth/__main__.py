"""
This module should be used as a Flask extension.
"""


import sys


if __name__ == '__main__':
    print("This module should not be called directly.")
    sys.exit(1)
