"""
A sample Flask app to ilustrate the use of the flask_jwtauth extension
"""

from flask import Flask, request
from flask_jwtauth import JWTAuth, current_user
from database import init_db, get_session
from user_model import User


auth = JWTAuth()


@auth.register_auth_method
def auth_method():
    credentials = request.get_json()
    user = None
    if 'username' in credentials and 'password' in credentials:
        with get_session(expire_on_commit = False) as session:
            user = session.query(User).filter(User.username == credentials['username']).one_or_none()
        if not user:
            return None
        if user.auth(credentials['password']):
            return user
    return None


@auth.register_get_user
def get_user(user_id):
    user = None
    with get_session(expire_on_commit = False) as session:
        user = session.query(User).filter(User.id == user_id).one_or_none()
    return user


def create_app(config = None):
    app = Flask(__name__, instance_relative_config = True)

    if config is None:
        env = app.config.get('ENV', 'production')
        if env == 'development':
            app.config.from_object('config.Development')
        elif env == 'production':
            app.config.from_object('config.Production')
        else:
            app.config.from_object('config.Config')
    else:
        app.config.from_mapping(config)

    init_db(app)
    auth.init_app(app)


    @app.route('/', methods = ['GET'])
    def index():
        return {"message": "Hello World!"}


    @app.route('/create_user', methods = ['POST'])
    def create_user():
        data = request.get_json()
        if ('username' in data and 'password' in data):
            new_user = User()
            new_user.username = data['username']
            new_user.set_password(data['password'])
            if 'inactive' in data and data['inactive']:
                new_user.is_active = False
            try:
                with get_session() as session:
                    session.add(new_user)
            except RuntimeError:
                return ({'message': 'was not possible to create the user'}, 500)
            return ({'message': 'user created'}, 201)
        return ({'message': 'username and password fields are mandatory'}, 400)


    @app.route('/protected', methods = ['GET'])
    @auth.auth_required
    def protected():
        return {
            'message': 'This is a protected area',
            'username': current_user.username
        }


    app.add_url_rule(
        rule = '/login',
        view_func = auth.auth_view,
        methods = ['POST']
    )


    app.add_url_rule(
        rule = '/logout',
        view_func = auth.logout_view,
        methods = ['GET']
    )


    return app
