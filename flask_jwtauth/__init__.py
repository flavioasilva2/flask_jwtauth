"""
The Flask-JWTAuth module
"""


from datetime import datetime as _datetime
from functools import wraps as _wraps
from json import dumps as _dumps, loads as _loads
from werkzeug.local import LocalProxy as _LocalProxy
from flask import (
    request as _request,
    make_response as _make_response,
    current_app as _current_app,
    _request_ctx_stack
)
from jwcrypto.jwt import JWT as _JWT
from jwcrypto.jwk import JWK as _JWK


class JWTAuth():
    """
    A JWT based auth extension for flask.

    Configuration parameters:

    JWTAUTH_KEY: the key to sign the token
    JWTAUTH_MAX_AGE: the max age in seconds to the token to live.
    """

    __app = None
    __auth_method = None
    __get_user_method = None


    def __init__(self, app = None):
        self.__app = app
        if app is not None:
            self.init_app(app)


    def init_app(self, app):
        """
        Registers the module with app when the app instance cannot be passed as
        a constructor argument.
        """

        self.__app = app


    def register_auth_method(self, func):
        """
        This method registers the function that recives the POST auth  request,
        and is intended to be used as an annotation. The annoted function  must
        return the user object on success or None in case of failure. The  user
        object must contain at least the following attributes:

        id: int or str
        is_active: bool
        """

        self.__auth_method = func
        return func


    def register_get_user(self, func):
        """
        This method is intended to be used  as an  annotation, it  records  the
        function responsible for getting the user object on post  login  access
        validation.  The  returned  user  object  must  contain  at  least  the
        following attributes:

        id: int or str
        is_active: bool
        """
        self.__get_user_method = func
        return func


    @staticmethod
    def __get_token_times():
        now = int(_datetime.now().timestamp())
        expires = now + int(_current_app.config['JWTAUTH_MAX_AGE'])
        return (now, expires)


    @staticmethod
    def __generate_token(user_id, token_times):
        if not _current_app.config.get('JWTAUTH_KEY'):
            raise KeyError("the config key JWTAUTH_KEY must be set.")
        json_key = {
            "k": _current_app.config['JWTAUTH_KEY'],
            "kty":"oct"
        }
        key = _JWK.from_json(_dumps(json_key))
        (issued_at_timestamp, expiration_timestamp) = token_times
        token = _JWT(
            header = {
                "alg": "HS256"
            },
            claims = {
                "user_id": user_id,
                "iat": issued_at_timestamp,
                "exp": expiration_timestamp
            }
        )
        token.make_signed_token(key)
        return token.serialize()


    @property
    def auth_view(self):
        """
        The function returned by this property must be registred as the  login
        route in the flask app. The only accepted HTTP method must be POST. In
        order  to  this  property  work  correctly  the  auth_method  must  be
        registered with register_auth_method.
        """

        def auth_view_func():
            if _request.method == 'POST':
                if not self.__auth_method:
                    raise RuntimeError("Please register the auth_method with register_auth_method.")
                user_object = self.__auth_method()
                if not user_object:
                    return ({"login_message": "authentication failed"}, 401)
                if not user_object.is_active:
                    return ({"login_message": "user is not active"}, 401)
                response = _make_response({"login_message": "authentication sucess"}, 200)
                token_times = self.__get_token_times()
                token = self.__generate_token(user_object.id, token_times)
                secure_cookie = not _current_app.config.get('DEBUG')
                (_, token_exp_timestamp) = token_times
                response.set_cookie(
                    key = "auth_token",
                    value = token,
                    expires = token_exp_timestamp,
                    httponly = True,
                    secure = secure_cookie,
                )
                return response
            return ({'login_message': 'The only accepted method is POST'}, 405)
        return auth_view_func


    def auth_required(self, func):
        """
        This method is intended to be used as an annotation to view functions  that  need
        authintication.
        """

        @_wraps(func)
        def wrapper(*args, **kwargs):
            if not _current_app.config.get('JWTAUTH_KEY'):
                raise KeyError("the config key JWTAUTH_KEY must be set.")
            json_key = {
                "k": _current_app.config['JWTAUTH_KEY'],
                "kty":"oct"
            }
            key = _JWK.from_json(_dumps(json_key))
            token = _request.cookies.get('auth_token')
            if not token:
                return ({"login_message": "user not authenticated"}, 401)
            token_object = _loads(_JWT(key=key, jwt=token).claims)
            user_id = token_object['user_id']
            if not self.__get_user_method:
                raise RuntimeError("Please register the get_user method with register_get_user.")
            user = self.__get_user_method(user_id)
            if not user:
                return ({"login_message": "error getting user"}, 401)
            if not user.is_active:
                return ({"login_message": "user is not active"}, 401)
            ctx = _request_ctx_stack.top
            if ctx is not None:
                setattr(ctx, 'current_user', user)
            return func(*args, **kwargs)
        return wrapper


    @property
    def logout_view(self):
        """
        The function returned by this property must be registred as the  logout
        route in the flask app. The only accepted HTTP method must be GET.
        """

        def logout_view_func():
            if _request.method == 'GET':
                response = _make_response()
                response.set_cookie('auth_token', '', expires=0)
                return response
            return ({'login_message': 'The only accepted method is GET'}, 405)
        return logout_view_func


def _lookup_user():
    """
    This method retrives  the  user  from  request  stack  to  be  used  in
    'current_user' proxy. Internal use only
    """

    top = _request_ctx_stack.top
    if top is None:
        raise RuntimeError("Cannot use current_user proxy outside a request.")
    return getattr(top, 'current_user')

current_user = _LocalProxy(_lookup_user)

__all__ = [
    'JWTAuth',
    'current_user',
]
