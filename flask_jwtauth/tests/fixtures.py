from collections import namedtuple
from flask import Flask
from pytest import fixture


User = namedtuple('User', 'username password id is_active')


def create_app(config_object = None):
    """A minimal flask application factory to test the extension"""

    app = Flask(__name__)

    if config_object is not None:
        app.config.from_mapping(config_object)

    return app


@fixture
def App():
    """Return a configured test app instance"""

    return create_app({
        'TESTING': True,
        'JWTAUTH_KEY': 'key',
        'JWTAUTH_MAX_AGE': 600
    })


@fixture
def client(App):
    """Flask test client"""

    return App.test_client()

@fixture
def users():
    return [
        User(
            username = 'foo',
            password = 'foo_password',
            id = 1,
            is_active = True
        ),
        User(
            username = 'bar',
            password = 'bar_password',
            id = 2,
            is_active = False
        )
    ]
