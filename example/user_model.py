# For production use bcrypt or similar
from crypt import crypt
from hmac import compare_digest
from sqlalchemy import (
    Column,
    Integer,
    Boolean,
    String
)
from database import Base


class User(Base):
    __tablename__ = 'app_user'
    id = Column(Integer, primary_key = True)
    is_active = Column(Boolean(), default = True)
    username = Column(String(100), nullable = False, unique = True)
    password_hash = Column(String(106), nullable = False)

    def set_password(self, password):
        self.password_hash = crypt(password)

    def auth(self, password):
        return compare_digest(crypt(password, self.password_hash), self.password_hash)

    def __repr__(self):
        return f"<User id={self.id}, is_active={self.is_active}, \
            username={self.username}, password_hash={self.password_hash}>"