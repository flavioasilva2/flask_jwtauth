import os

def str_to_bool(s):
    if isinstance(s, bool):
        return s
    elif s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise RuntimeError(f"Cannot convert {s} to bool")

class Config():
    """
    Common config across environments
    """
    pass


class Development(Config):
    """
    Development configs
    """
    JWTAUTH_KEY = 'dev_key'
    JWTAUTH_MAX_AGE = 600 # 10 min
    ORM_ECHO = True
    DATABASE_URL = 'sqlite:///test.db'


class Production(Config):
    """
    Production configs
    """
    JWTAUTH_KEY = os.environ.get("JWTAUTH_KEY")
    JWTAUTH_MAX_AGE = int(os.environ.get("JWTAUTH_MAX_AGE", 0))
    ORM_ECHO = str_to_bool(os.environ.get("ORM_ECHO", False))
