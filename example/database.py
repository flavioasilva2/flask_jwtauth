from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.pool import StaticPool

Base = declarative_base()

_Session = None

def init_db(app):
    global _Session

    engine = None
    if 'DATABASE_URL' in app.config:
        engine = create_engine(
            app.config['DATABASE_URL'],
            echo = app.config['ORM_ECHO']
        )
    else:
        engine = create_engine(
            'sqlite:///',
            echo = app.config['ORM_ECHO'],
            connect_args = {"check_same_thread": False},
            poolclass = StaticPool
        )
    
    _Session = scoped_session(
        sessionmaker(
            autocommit = False,
            autoflush = False,
            bind = engine
        )
    )

    Base.query = _Session.query_property()

    # Import all database models here
    from user_model import User

    Base.metadata.create_all(bind=engine)


@contextmanager
def get_session(expire_on_commit = True):
    global _Session
    if not _Session:
        raise RuntimeError("Session object is None, did you called init_db ?")
    session = _Session()
    if not expire_on_commit:
        session.expire_on_commit = False
    try:
        yield session
        session.commit()
    except Exception as exc:
        session.rollback()
        raise RuntimeError('An error occurred while the db session was in use, the last transaction was rolled-back') from exc
    finally:
        session.close()
