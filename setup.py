from setuptools import setup, find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='Flask-JWTAuth',
    version='0.3.0',
    url='https://gitlab.com/flavioasilva2/flask_jwtauth',
    license='MIT',
    author='Flávio Amaral e Silva',
    author_email='flavio.asilva@gmail.com',
    description='A jwt based auth extension for flask',
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'flask>=1.0.0',
        'jwcrypto>=0.8'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    python_requires='>=3.6'
)

