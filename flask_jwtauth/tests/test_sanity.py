from fixtures import create_app


def test_app_config_sanity():
    """Test the app configuration sanity"""

    assert create_app() is not None

    app = create_app()
    assert app.config.get('TESTING') == False

    app = create_app({'TESTING': True})
    assert app.config.get('TESTING') == True
