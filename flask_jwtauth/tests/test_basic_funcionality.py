from fixtures import client, App, users
from flask_jwtauth import JWTAuth, current_user
from flask import request


def register_callbacks(auth, users):
    @auth.register_auth_method
    def do_auth():
        credentials = request.get_json()
        for u in users:
            if (u.username == credentials['username']) and (u.password == credentials['password']):
                return u
        return None
        
    @auth.register_get_user
    def get_user(user_id):
        for u in users:
            if u.id == user_id:
                return u
        return None

def test_simple_auth(App, client, users):
    """Simple test case"""
    
    auth = JWTAuth(App)
    register_callbacks(auth, users)
    
    App.add_url_rule('/login', methods=['POST'], view_func=auth.auth_view)

    @App.route('/test')
    @auth.auth_required
    def protected():
        return ({'current_user': current_user.username})

    result = client.get('/test')

    assert result.status_code == 401
    
    response_json = result.get_json()
    assert response_json['login_message'] == 'user not authenticated'

    client.post(
        '/login',
        json={
            'username': 'foo',
            'password': 'foo_password',
        }
    )

    result = client.get('/test')

    assert result.status_code == 200
    
    response_json = result.get_json()
    assert response_json['current_user'] == 'foo'


def test_logout(App, client, users):
    """Test if logout works"""
    
    auth = JWTAuth(App)
    register_callbacks(auth, users)
    
    App.add_url_rule('/login', methods=['POST'], view_func=auth.auth_view)
    App.add_url_rule('/logout', methods=['GET'], view_func=auth.logout_view)

    @App.route('/test')
    @auth.auth_required
    def protected():
        return ({'current_user': current_user.username})

    client.post(
        '/login',
        json={
            'username': 'foo',
            'password': 'foo_password',
        }
    )

    result = client.get('/test')

    assert result.status_code == 200
    
    response_json = result.get_json()
    assert response_json['current_user'] == 'foo'

    client.get('/logout')

    result = client.get('/test')

    assert result.status_code == 401
    
    response_json = result.get_json()
    assert response_json['login_message'] == 'user not authenticated'
