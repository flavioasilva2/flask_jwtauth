# Flask-JWTAuth

> Note: This extension is in early development phase.

A Flask auth extension based on json web tokens

## Development mode install

First create the virtualenv
```
python3 -m venv .venv
```

Activate the virtualenv
```
. ./.venv/bin/activate
```

Now install/update setuptools, wheel and pip
```
pip install -U setuptools wheel pip
```

Finally install the package as editable
```
pip install -e .
```

Optionally install the testing dependencies

```
pip install -r requirements.txt
```

## How to build

First perform the development install

Now build
```
python setup.py sdist bdist_wheel
```

The build artefacts are on build dir

## How to test

First perform the development install

Now run the tests with:
```
pytest
```

## How to run the example app

First perform the development install

Now

```
cd example
```

Install the example app requirements

```
pip install -r requirements.txt
```

Now run the example app

```
FLASK_ENV=development FLASK_APP=app:create_app flask run
```

The accepted requests are:

GET /

POST /create_user {'username': str, 'password': str, 'inactive': optional str}

GET /protected

POST /login {'username': str, 'password': str}

GET /logout